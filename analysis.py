import math
import pickle
from functools import reduce

from old_files.TruthFinding import TruthFinding


def number_of_values():
    truthFinder = TruthFinding()
    # argTruth.read_file("input2.json")
    # argTruth.load_pickle("flight_data.p")
    # argTruth.read_json("my.json")
    truthFinder.read_all_date("Data/data.json")
    __dates = ["2011-12-%02d" % i for i in range(1, 32)]
    __dates.remove("2011-12-06")
    __dates.remove("2011-12-21")
    __dates.remove("2011-12-23")
    __dates.extend(["2012-01-%02d" % i for i in range(1, 4)])

    __item_name = ["sch_depart", "act_depart", "depart_gate",
                   "sch_arrive", "act_arrive", "arrive_gate"]

    __number_of_values = {}
    __number_of_values_all = {}
    for d in __item_name:
        __number_of_values[d] = []
        __number_of_values_all[d] = []
    __number_of_values_by_day = {}

    for the_day in __dates:
        truthFinder.set_date(the_day)
        truthFinder.reset()
        truthFinder.pre_processing_by_source()

        __number_of_values = {}
        for d in __item_name:
            __number_of_values[d] = []

        for item in truthFinder.Vd:
            for d in __item_name:
                if item.find(d) != -1:
                    __number_of_values[d].append(len(truthFinder.Vd[item]))
                    __number_of_values_all[d].append(len(truthFinder.Vd[item]))

        __number_of_values_by_day[the_day] = {}
        for d in __item_name:
            l = __number_of_values[d]
            __number_of_values_by_day[the_day][d] = reduce(lambda x, y: x + y, l) / len(l)

    for d in __item_name:
        l = __number_of_values_all[d]
        __number_of_values_all[d] = reduce(lambda x, y: x + y, l) / len(l)  # average
        print(d, '\t', __number_of_values_all[d])


def entropy():
    truthFinder = TruthFinding()
    # argTruth.read_file("input2.json")
    # argTruth.load_pickle("flight_data.p")

    truthFinder.read_all_date("Data/data.json")
    __dates = ["2011-12-%02d" % i for i in range(1, 32)]
    __dates.remove("2011-12-06")
    __dates.remove("2011-12-21")
    __dates.remove("2011-12-23")
    __dates.extend(["2012-01-%02d" % i for i in range(1, 4)])

    __entropy_all = {}

    for the_day in __dates:
        truthFinder.set_date(the_day)
        truthFinder.reset()
        truthFinder.pre_processing_by_source()

        for d in truthFinder.Vd:
            if d not in __entropy_all:
                __entropy_all[d] = []
            __sum = 0
            for v in truthFinder.Vd[d]:
                __sum -= 1.0 *  len(truthFinder.Sv[v]) / len(truthFinder.Sd[d]) * \
                    math.log(1.0 * len(truthFinder.Sv[v]) / len(truthFinder.Sd[d]))
            __entropy_all[d].append(__sum)

    __item_name = ["sch_depart", "act_depart", "depart_gate",
                   "sch_arrive", "act_arrive", "arrive_gate"]

    __entropy = {}
    for d in __item_name:
        __entropy[d] = []

    for item in __entropy_all:
        for d in __item_name:
            if item.find(d) != -1:
                __entropy[d].extend(__entropy_all[item])

    for d in __item_name:
        l = __entropy[d]
        print(d, '\t', reduce(lambda x, y: x + y, l) / len(l) )

    __dbg = None  # Debug point

def source_accuracy():
    gold_std = pickle.load(open("flight_truth.p", "rb"))
    __dates = ["2011-12-%02d" % i for i in range(1, 32)]
    __dates.remove("2011-12-06")
    __dates.remove("2011-12-21")
    __dates.remove("2011-12-23")
    __dates.extend(["2012-01-%02d" % i for i in range(1, 4)])
    truthFinder = TruthFinding()
    truthFinder.read_all_date("Data/data.json")
    S = pickle.load(open("S.p", "rb"))  # load set of sources
    accuracy = {}
    for the_day in __dates:
        accuracy[the_day] = {}
        truthFinder.set_date(the_day)
        truthFinder.reset()
        truthFinder.pre_processing_by_source()

        __gold_of_date = gold_std[the_day]
        source_data = truthFinder.data

        for s in S:
            __total = 0
            __correct = 0
            for t in source_data[s]:
                if t in __gold_of_date:
                    # print(t, "gold:", __gold_of_date[t], "found:", source_data[t])
                    if s not in accuracy[the_day]:
                        accuracy[the_day][s] = []

                    __total += 1
                    __found = source_data[s][t]
                    __gold = __gold_of_date[t]
                    if __found == __gold:
                        __correct += 1
            if __total != 0:
                accuracy[the_day][s].append(1.0 * __correct / __total)

    accuracy_all = {}
    for s in S:
        accuracy_all[s] = []

    for the_day in __dates:
        for s in S:
            if s in accuracy[the_day]:
                accuracy_all[s].extend(accuracy[the_day][s])

    for s in S:
        l = accuracy_all[s]
        print(s, '\t', reduce(lambda x, y: x + y, l) / len(l))
    __dbg = None  # Debug point

if __name__ == "__main__":
    source_accuracy()
   # __dbg = None  # Debug point
        # number of values

