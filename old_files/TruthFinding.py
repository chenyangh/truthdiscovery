import json
import math
import pickle


class TruthFinding:

    def __init__(self):
        self.data = []
        self.all_date = []
        self.S = set()  # set of all source
        self.V = set()  # set of all values
        self.D = set()  # set of all data items
        self.Sv = {}  # set of sources providing value v
        self.Sd = {}  # set of sources providing a value for data item d
        self.Ds = {}  # set of data items covered by source s
        self.Dv = {}  # set of data items corresponding to value v
        self.Vs = {}  # set of values provided by source s
        self.Vd = {}  # set of values provided for data item d
        self.default_num_iterations = 5
        self.Ts = {}
        self.Cv = {}

    def read_json(self, file_name):
        # Read data
        with open(file_name) as data_file:
            self.data = json.load(data_file)
        # print(self.data)

    def read_all_date(self, file_name):
        with open(file_name) as data_file:
            self.all_date = json.load(data_file)

    def set_date(self, date):
        self.data = self.all_date[date]

    def read_by_date(self, file_name, date):
        with open(file_name) as data_file:
            all_data = json.load(data_file)
        self.data = all_data[date]

    def load_pickle(self, p_name):
        tmp_data = pickle.load(open(p_name, "rb"))
        self.data = {}
        for item in tmp_data:
            if tmp_data[item] != {}:
                self.data[item] = tmp_data[item]

    def pre_processing_by_item(self):
        for d in self.data:
            # D

            self.D.add(d)

            for s in self.data[d]:
                # S
                self.S.add(s)

                # Sd
                if d not in self.Sd:
                    self.Sd[d] = set()
                    self.Sd[d].add(s)
                else:
                    self.Sd[d].add(s)

                # Ds
                if s not in self.Ds:
                    self.Ds[s] = set()
                    self.Ds[s].add(d)
                else:
                    self.Ds[s].add(d)

                v = self.data[d][s]

                if v.find('empty') != -1:  # Ignore empty values
                    continue

                # V
                self.V.add(v)

                # Sv
                if v not in self.Sv:
                    self.Sv[v] = set()
                    self.Sv[v].add(s)
                else:
                    self.Sv[v].add(s)

                # Dv
                if v not in self.Dv:
                    self.Dv[v] = set()
                    self.Dv[v].add(d)
                else:
                    self.Dv[v].add(d)

                # Vd
                if d not in self.Vd:
                    self.Vd[d] = set()
                    self.Vd[d].add(v)
                else:
                    self.Vd[d].add(v)

                # Vs
                if s not in self.Vs:
                    self.Vs[s] = set()
                    self.Vs[s].add(v)
                else:
                    self.Vs[s].add(v)


    def pre_processing_by_source(self):
        for s in self.data:
            self.S.add(s)  # get set of all source
            for d in self.data[s]:  # get set of all values
                self.D.add(d)
                if d in self.Sd:  # Checked
                    self.Sd[d].add(s)
                else:
                    self.Sd[d] = set()
                    self.Sd[d].add(s)

                if s in self.Ds:
                    self.Ds[s].add(d)
                else:
                    self.Ds[s] = set()
                    self.Ds[s].add(d)

                v = self.data[s][d]  #
                if v.find("empty") == -1:  # not empty
                    self.V.add(v)
                    if v in self.Sv:  # Checked
                        self.Sv[v].add(s)
                    else:
                        self.Sv[v] = set()
                        self.Sv[v].add(s)

                    if v in self.Dv:
                        self.Dv[v].add(d)
                    else:
                        self.Dv[v] = set()
                        self.Dv[v].add(d)

                    if d in self.Vd:
                        self.Vd[d].add(v)
                    else:
                        self.Vd[d] = set()
                        self.Vd[d].add(v)

                    if s in self.Vs:
                        self.Vs[s].add(v)
                    else:
                        self.Vs[s] = set()
                        self.Vs[s].add(v)

        self.num_data_items = len(self.D)
        self.num_sources = len(self.S)
        self.num_values = len(self.V)

    def reset_Ts_Cv(self):
        for s in self.S:
            self.Ts[s] = 0
        for v in self.V:
            self.Cv[v] = 0

    def reset(self):
        self.S = set()  # set of all source
        self.V = set()  # set of all values
        self.D = set()
        self.Sv = {}  # set of sources providing value v
        self.Sd = {}  # set of sources providing a value for data item d
        self.Ds = {}  # set of data items covered by source s
        self.Dv = {}  # set of data items corresponding to value v
        self.Vs = {}  # set of values provided by source s
        self.Vd = {}  # set of values provided for data item d
        self.default_num_iterations = 5
        self.Ts = {}
        self.Cv = {}

    def calculate_value_confidence(self, num_iterations=None, method=None):
        # the methods can be : "sum", "average_log", "investment", "pooled_investment",
        # "truth_finder", "cosine", "2_estimates", "3_estimates"
        method_name = 'method_' + method
        func = getattr(self, method_name, lambda: "nothing")
        return func(num_iterations)

    def method_sum(self, num_iterations=None):
        self.reset_Ts_Cv()

        if num_iterations is None:
            num_iterations = self.default_num_iterations

        for v in self.V:
            self.Cv[v] = 1

        for ith_itr in range(num_iterations):
            for s in self.S:
                sum__ = 0
                for v in self.Vs[s]:
                    sum__ += self.Cv[v]
                self.Ts[s] = sum__

            for v in self.V:
                sum__ = 0
                for s in self.Sv[v]:
                    sum__ += self.Ts[s]
                self.Cv[v] = sum__

    def method_average_log(self, num_iterations=None):
        self.reset_Ts_Cv()

        if num_iterations is None:
            num_iterations = self.default_num_iterations

        for v in self.V:
            self.Cv[v] = 0.5

        for ith_itr in range(num_iterations):
            for s in self.S:
                sum__ = 0.0
                for v in self.Vs[s]:
                    sum__ += self.Cv[v]
                self.Ts[s] = 1.0 * sum__ * math.log(len(self.Vs[s])) / len(self.Vs[s])

            for v in self.V:
                sum__ = 0.0
                for s in self.Sv[v]:
                    sum__ += self.Ts[s]
                self.Cv[v] = sum__

    def method_investment(self, num_iterations=None):
        self.reset_Ts_Cv()

        if num_iterations is None:
            num_iterations = self.default_num_iterations

        for v in self.V:
            sum__ = 0.0
            for d in self.Dv[v]:
                sum__ += len(self.Sd[d])
            self.Cv[v] = 1.0 * len(self.Sv[v]) / sum__

        for s in self.S:
            self.Ts[s] = 1  # NOTE: this is not mentioned in paper

        for ith_itr in range(num_iterations):
            for s in self.S:
                sum__ = 0.0
                for v in self.Vs[s]:
                    sum2__ = 0.0
                    for r in self.Sv[v]:
                        sum2__ += 1.0 * self.Ts[r] / len(self.Vs[r])
                    sum__ += 1.0 * self.Cv[v] * self.Ts[s] / len(self.Vs[s]) / sum2__
                self.Ts[s] = sum__

            for v in self.V:
                sum__ = 0.0
                for s in self.Sv[v]:
                    sum__ += 1.0 * self.Ts[s] / len(self.Vs[s])
                self.Cv[v] = sum__

    def method_pooled_investment(self, num_iterations=None):
        self.reset_Ts_Cv()

        if num_iterations is None:
            num_iterations = self.default_num_iterations

        for v in self.V:
            self.Cv[v] = 1.0 / len(self.Dv[v])  # NOTE: using Dv instead of Vd

        for s in self.S:
            self.Ts[s] = 1  # NOTE: this is not mentioned in paper

        Hv = {}

        for ith_itr in range(num_iterations):
            for s in self.S:
                sum__ = 0.0
                for v in self.Vs[s]:
                    sum2__ = 0.0
                    for r in self.Sv[v]:
                        sum2__ += 1.0 * self.Ts[r] / len(self.Vs[r])
                    sum__ += 1.0 * self.Cv[v] * self.Ts[s] / len(self.Vs[s]) / sum2__
                self.Ts[s] = sum__

            for v in self.V:
                sum__ = 0.0
                for s in self.Sv[v]:
                    sum__ += 1.0 * self.Ts[s] / len(self.Vs[s])
                Hv[v] = sum__

            for v in self.V:
                sum__ = 0.0
                for d in self.Dv[v]:  # NOTE: the usage of Vd is not properly defined in paper
                    break
                for r in self.Vd[d]:
                    sum__ += math.pow(Hv[r], 1.4)
                self.Cv[v] = 1.0 * Hv[v] * math.pow(Hv[v], 1.4) / sum__

    def method_truth_finder(self, num_iterations=None):
        self.reset_Ts_Cv()

        gamma = 0.6  # NOTE: to be determined

        if num_iterations is None:
            num_iterations = self.default_num_iterations

        for s in self.S:
            self.Ts[s] = 0.8  # NOTE: this is not mentioned in paper

        for ith_itr in range(num_iterations):
            for v in self.V:
                sum__ = 0.0
                for s in self.Sv[v]:
                    sum__ += -1.0 * (math.log(1.0 - self.Ts[s]))
                self.Cv[v] = sum__
            # print(self.Ts)
            for s in self.S:
                sum__ = 0.0
                for v in self.Vs[s]:
                    sum__ += (1 - math.exp(-gamma * self.Cv[v]))
                self.Ts[s] = 1.0 * sum__ / len(self.Vs[s])

    def method_cosine(self, num_iterations=None):
        self.reset_Ts_Cv()

        if num_iterations is None:
            num_iterations = self.default_num_iterations

        for v in self.V:
            self.Cv[v] = 1.0

        for s in self.S:
            self.Ts[s] = 1.0
        for ith_itr in range(num_iterations):
            for s in self.S:
                sum1__ = 0.0
                for v in self.Vs[s]:
                    sum1__ += self.Cv[v]

                sum2__ = 0.0
                for d in self.Ds[s]:
                    for v in [item for item in self.Vd[d] if item not in self.Vs[s]]:
                        sum2__ += self.Cv[v]

                sum3__ = 0.0
                for d in self.Ds[s]:
                    sum3__ += len(self.Vd[d])

                sum4__ = 0.0
                for d in self.Ds[s]:
                    for v in self.Vd[d]:
                        sum4__ += math.pow(self.Cv[v],2)
                self.Ts[s] = 0.8 * self.Ts[s] + 0.2 * (sum1__ - sum2__) / math.sqrt(sum3__ * sum4__)

            for v in self.V:
                sum1__ = 0.0
                for s in self.Sv[v]:
                    sum1__ += math.pow(self.Ts[s], 3)

                sum2__ = 0.0
                for d in self.Dv[v]:
                    break
                for s in [item for item in self.Sd[d] if item not in self.Sv[v]]:
                    sum2__ += math.pow(self.Ts[s], 3)

                sum3__ = 0.0
                for s in self.Sd[d]:
                    sum3__ += math.pow(self.Ts[s], 3)

                # print(v, sum3__)
                self.Cv[v] = 1.0 * (sum1__ - sum2__) / sum3__

    def method_2_estimates(self, num_iterations=None):
        self.reset_Ts_Cv()

        if num_iterations is None:
            num_iterations = self.default_num_iterations

        for s in self.S:
            self.Ts[s] = 1.0

        for ith_itr in range(num_iterations):
            for v in self.V:
                sum1__ = 0.0
                for s in self.Sv[v]:
                    sum1__ += self.Ts[s]

                sum2__ = 0.0
                for d in self.Dv[v]:
                    break
                for s in [item for item in self.Sd[d] if item not in self.Sv[v]]:
                    sum2__ += (1 - self.Ts[s])

                sum3__ = len(self.Sd[d])

                self.Cv[v] = 1.0 * (sum1__ + sum2__) / sum3__

            for s in self.S:
                sum1__ = 0.0
                for v in self.Vs[s]:
                    sum1__ += self.Cv[v]

                sum2__ = 0.0
                for d in self.Ds[s]:
                    for v in [item for item in self.Vd[d] if item not in self.Vs[s]]:
                        sum2__ += (1 - self.Cv[v])

                sum3__ = 0.0
                for d in self.Ds[s]:
                    sum3__ += len(self.Vd[d])

                self.Ts[s] = 1.0 * (sum1__ + sum2__) / sum3__

    def method_3_estimates(self,  num_iterations=None):
        self.reset_Ts_Cv()

        if num_iterations is None:
            num_iterations = self.default_num_iterations

        Tv = {}
        for v in self.V:
            Tv[v] = 0.9
            self.Cv[v] = 0.0
        for s in self.S:
            self.Ts[s] = 1.0

        for ith_itr in range(num_iterations):
            for s in self.S:
                sum1__ = 0.0
                for v in self.Vs[s]:
                    sum1__ += 1.0 * self.Cv[v] / (1 - Tv[v])

                sum2__ = 0.0
                for d in self.Ds[s]:
                    for v in [item for item in self.Vd[d] if item not in self.Vs[s]]:
                        sum2__ += 1.0 * (1 - self.Cv[v]) / (1 - Tv[v])

                sum3__ = 0.0
                for d in self.Ds[s]:
                    sum3__ += len(self.Vd[d])

                self.Ts[s] = 1.0 * (sum1__ + sum2__) / sum3__

            for v in self.V:
                sum1__ = 0.0
                for s in self.Sv[v]:
                    sum1__ += 1.0 * self.Cv[v] / (1 - self.Ts[s])

                for d in self.Dv[v]:  # the corresponding d of v
                    break

                sum2__ = 0.0
                for s in [item for item in self.Sd[d] if item not in self.Sv[v]]:
                    sum2__ += 1.0 * (1 - self.Cv[v]) / (1 - self.Ts[s])

                sum3__ = len(self.Sd[d])

                Tv[v] = 1.0 * (sum1__ + sum2__) / sum3__

            for v in self.V:
                sum1__ = 0.0
                for s in self.Sv[v]:
                    sum1__ += 1.0 * self.Ts[s] * Tv[v]

                for d in self.Dv[v]:  # the corresponding d of v
                    break

                sum2__ = 0.0
                for s in [item for item in self.Sd[d] if item not in self.Sv[v]]:
                    sum2__ += 1.0 * (1 - self.Ts[s] * Tv[v])

                sum3__ = len(self.Sd[d])

                Tv[v] = 1.0 * (sum1__ + sum2__) / sum3__

    def method_simple_LCA(self, num_iterations=None):
        self.reset_Ts_Cv()

        for s in self.S:
            self.Ts[s] = 0.8

        beta1 = 0.8

        for K in range(20):
            for d in self.D:
                Cd_sum = 0
                for v in self.Vd:
                    self.Cv[v] = 1

    def get_truth(self):
        tmp_truth = {}
        # print(self.Cv)
        for d in self.D:
            if d not in self.Vd:
                #print(d, "is not provided by any source")
                continue
            tmp_max = 0
            tmp_value = ""
            for v in self.Vd[d]:
                if self.Cv[v] > tmp_max:
                    tmp_max = self.Cv[v]
                    tmp_value = v
            # print("Max value for", d, "is", tmp_value)
            tmp_truth[d] = tmp_value
        return tmp_truth

    @staticmethod
    def compare_truth(__found_truth, __gold_std):
        __total = 0
        __correct = 0
        for t in found_truth:
            if t in __gold_std:
                __found = __found_truth[t]
                __gold = __gold_std[t]
                if __gold.find("empty") != -1:
                    continue
                __total += 1
                if __found == __gold:
                    __correct += 1
                else:
                    print(t, "gold:", __gold_std[t], "found:", __found_truth[t])
        return 1.0 * __correct / __total

if __name__ == "__main__":
    argTruth = TruthFinding()

    argTruth.read_json("data/data_by_item.json")
    argTruth.pre_processing_by_item()
    #pickle.dump(argTruth, open("data/argTruth.p", "wb"))
    #argTruth = pickle.load(open("data/argTruth.p", "rb"))
    gold_std = pickle.load(open("data/gold_by_item.p", "rb"))

    # the methods can be : "sum", "average_log", "investment", "pooled_investment",
    # "truth_finder", "cosine", "2_estimates", "3_estimates"
    methods_list = ["sum", "average_log", "investment", "pooled_investment",
     "truth_finder", "cosine", "2_estimates", "3_estimates"]

    for num_itr in range(2):
        argTruth.calculate_value_confidence(num_iterations=num_itr, method="average_log")
        # the methods can be : "sum", "average_log", "investment", "pooled_investment",
        # "truth_finder", "cosine", "2_estimates", "3_estimates"

        found_truth = argTruth.get_truth()


        _accuracy = argTruth.compare_truth(found_truth, gold_std)
        print(_accuracy)

    print("Number of values:", len(argTruth.Cv))
