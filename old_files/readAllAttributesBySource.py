from os import listdir
from os.path import join, splitext
import codecs
import pickle
from dateutil import parser
import re
import datetime
import json
import os


error_log_file = "error_log.txt"
with open(error_log_file, "w") as f3:
    f3.write("")
CLASSPATH = '/home/chenyang/Downloads/stanford-ner-2015-12-09'
os.environ['CLASSPATH'] = CLASSPATH

def get_flight_values():
    file_path = "clean_flight"
    list_of_files = [f for f in listdir(file_path)]

    S = pickle.load(open("/data/S.p", "rb"))  # load set of sources

    flight_data = {}

    for file_name in list_of_files:
        # file_name = "2011-12-02-data.txt"
        current_date = splitext(file_name)[0][:-5]

        current_datetime = parser.parse(current_date)
        previous_day_to_remove = (current_datetime-datetime.timedelta(1)).strftime("%m/%d")
        next_day_to_remove = (current_datetime+datetime.timedelta(1)).strftime("%m/%d")

        previous_day_to_remove2 = re.sub(r"0(?=[0-9])", "", previous_day_to_remove)
        next_day_to_remove2 = re.sub(r"0(?!0)", "", next_day_to_remove)

        flight_data[current_date] = {}
        for s in S:
            flight_data[current_date][s] = {}
        file_to_open = join(file_path, file_name)

        print(current_date)
        with codecs.open(file_to_open, encoding='utf-8', errors='replace') as f:
            for line in f:
                split_line = line.split('\t')

                __source = split_line[0]
                __flight_num = split_line[1]

                __item_name = ["sch_depart", "act_depart", "depart_gate",
                               "sch_arrive", "act_arrive", "arrive_gate"]

                for i in [2, 3, 5, 6]:  # value contains time
                    __tmp_item_name = __flight_num + '-' + __item_name[i-2]

                    split_line[i] = re.sub("Delayed", "", split_line[i])
                    split_line[i] = re.sub("On Time", "", split_line[i])
                    split_line[i] = re.sub("pm", " pm ", split_line[i])
                    split_line[i] = re.sub("am", " am ", split_line[i])
                    split_line[i] = re.sub(previous_day_to_remove, "", split_line[i])
                    split_line[i] = re.sub(next_day_to_remove, "", split_line[i])
                    split_line[i] = re.sub(previous_day_to_remove2, "", split_line[i])
                    split_line[i] = re.sub(next_day_to_remove2, "", split_line[i])
                    split_line[i] = re.sub("noon", " p ", split_line[i])
                    split_line[i] = re.sub("\*", "", split_line[i])
                    split_line[i] = re.sub(r"\(.*\)", "", split_line[i])
                    split_line[i] = re.sub("Jan", " Jan", split_line[i])
                    split_line[i] = re.sub("Dec", " Dec", split_line[i])
                    split_line[i] = re.sub("Nov", " Nov", split_line[i])
                    split_line[i] = re.sub("AM", " AM ", split_line[i])
                    split_line[i] = re.sub("Not Available", "", split_line[i])
                    split_line[i] = re.sub("&nbsp", "", split_line[i])

                    try:
                        __parsed_date = parser.parse(split_line[i])
                        __tmp_item_value = __parsed_date.strftime("%H:%M")
                    except ValueError:
                        if split_line[i].strip() == "":
                            __tmp_item_value = "empty"
                        elif split_line[i].find("Cancelled") != -1 or \
                                        split_line[i].find("Canceled") != -1:
                            __tmp_item_value = "Cancelled"
                        elif split_line[i].find("Contact Airline") != -1:
                            __tmp_item_value = "empty"
                        else:
                            try:
                                __parsed_date = parser.parse(split_line[i])
                                __tmp_item_value = __parsed_date.strftime("%H:%M")
                            except ValueError:
                                __tmp_item_value = "empty"
                                with open(error_log_file, "a") as f3:
                                    f3.write(split_line[i])

                    flight_data[current_date][__source][__tmp_item_name] = __tmp_item_value

                for i in [4, 7]:  # Gate
                    __tmp_item_name = __flight_num + '-' + __item_name[i-2]
                    __tmp_item_value = split_line[i].strip()

                    if __tmp_item_value == "":
                        __tmp_item_value = "empty"
                    else:
                        try:
                            __tmp_item_value = re.findall(r'([0-9]+)', __tmp_item_value)[-1]
                        except:
                            __tmp_item_value = "empty"
                            with open(error_log_file, "a") as f3:
                                f3.write(split_line[i])

                    flight_data[current_date][__source][__tmp_item_name] = __tmp_item_value

    pickle.dump(flight_data, open("flight_data.p", "wb"))
    return flight_data


def get_flight_gold_std():
    file_path = "flight_truth"
    list_of_files = [f for f in listdir(file_path)]

    S = pickle.load(open("S.p", "rb"))  # load set of sources

    flight_data = {}

    for file_name in list_of_files:
        # file_name = "2011-12-02-data.txt"
        current_date = splitext(file_name)[0][:-6]

        flight_data[current_date] = {}

        file_to_open = join(file_path, file_name)

        print(current_date)
        with codecs.open(file_to_open, encoding='utf-8', errors='replace') as f:
            for line in f:
                split_line = line.split('\t')

                __flight_num = split_line[0]

                __item_name = ["sch_depart", "act_depart", "depart_gate",
                               "sch_arrive", "act_arrive", "arrive_gate"]

                for i in [1, 2, 4, 5]:  # value contains time
                    __tmp_item_name = __flight_num + '-' + __item_name[i - 1]

                    split_line[i] = re.sub("Delayed", "", split_line[i])
                    split_line[i] = re.sub("On Time", "", split_line[i])
                    split_line[i] = re.sub("pm", " pm ", split_line[i])
                    split_line[i] = re.sub("am", " am ", split_line[i])
                    split_line[i] = re.sub("11/30", "", split_line[i])
                    split_line[i] = re.sub("noon", " p ", split_line[i])
                    split_line[i] = re.sub("\*", "", split_line[i])
                    split_line[i] = re.sub(r"\(.*\)", "", split_line[i])
                    split_line[i] = re.sub("Dec", " Dec", split_line[i])
                    split_line[i] = re.sub("Nov", " Nov", split_line[i])
                    split_line[i] = re.sub("AM", " AM ", split_line[i])
                    split_line[i] = re.sub("Not Available", "", split_line[i])
                    split_line[i] = re.sub("&nbsp", "", split_line[i])

                    try:
                        __parsed_date = parser.parse(split_line[i])
                        __tmp_item_value = __parsed_date.strftime("%H:%M")
                    except:
                        if split_line[i].strip() == "":
                            __tmp_item_value = "empty"
                        elif split_line[i].find("Cancelled") != -1 or split_line[i].find("Canceled") != -1:
                            __tmp_item_value = "Cancelled"
                        elif split_line[i].find("Contact Airline") != -1:
                            __tmp_item_value = "empty"
                        else:
                            __tmp_item_value = "empty"
                            with open(error_log_file, "a") as f3:
                                f3.write(split_line[i])

                    flight_data[current_date][__tmp_item_name] = __tmp_item_value

                for i in [3, 6]:  # Gate
                    __tmp_item_name = __flight_num + '-' + __item_name[i - 1]
                    __tmp_item_value = split_line[i].strip()
                    if __tmp_item_value == "":
                        __tmp_item_value = "empty"
                    else:
                        try:
                            __tmp_item_value = re.findall(r'([0-9]+)', __tmp_item_value)[-1]
                        except:
                            __tmp_item_value = "empty"
                            with open(error_log_file, "a") as f3:
                                __tmp_item_value
                                f3.write(split_line[i])

                    flight_data[current_date][__tmp_item_name] = __tmp_item_value

    pickle.dump(flight_data, open("flight_truth.p", "wb"))
    return flight_data


def write_to_json(__data, file_name):
    # __data = pickle.load(open("flight_data.p", 'rb'))
    new_data = dict((k, v) for k, v in __data.items() if v != {})  # Remove empty entity
    with open(file_name, "w") as f:
        json.dump(new_data, f)

data = get_flight_values()
write_to_json(data, "Data/data_by_source.json")

data = get_flight_gold_std()
write_to_json(data, "Data/truth_by_source.json")
