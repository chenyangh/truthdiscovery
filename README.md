# Argument Based Truth Discovery Methods #

### What is it? ###

This repo provides implementations of well-known argument based truth discovery methods.
The algorithms are provided by [Veracity of Data:From Truth Discovery Computation Algorithms to Models of Misinformation Dynamics](http://www.morganclaypool.com/doi/abs/10.2200/S00676ED1V01Y201509DTM042)

Implemented algorithms are:
* Sum
* Average.Log
* Investment
* Pooled Investment
* TruthFinder
* Cosine
* 2-Estimates
* 3-Estimates

### Environment  ###

Code is writen in Python 3.4 but python 2.7 is also tested.

### How to use? ###

The methods are defined in Module ArgTruthCmp.py

First initiate an class instance
```python
import argTruthCmp
argTruth = argTruthCmp.ArgTruthCmp()
```

To read a json file :
```
argTruth.read_file("input2.json")
```

A prepossessing must be done once and only once.
```
argTruth.pre_processing()
```

Than calculate the confidence of each values.
```
argTruth.calculate_value_confidence(num_iterations=4, method="2_estimates")
```
Where in this function, you can specify the number of interactions and estimation methods.
Available options can be
```
"sum", "average_log", "investment", "pooled_investment", "truth_finder", "cosine", "2_estimates", "3_estimates"
```
Finally call
```
argTruth.get_value_for_items()
```
to give the most estimated value for each data item.

The example is provided in "example.py", format of input file is shown in "input.json"
```
{
	"s1":{
		"q1": "Obama",
		"q2": "Putin",
	    ...
	},
	"s2":{
		"q3": "Sarkozy",
	}
    ...
}
```
s1, s2 indicates the name of sources, q1, q2, ect. are the data items. "Obama", etc. are values corresponding to the data item.

### Flight Example ###

The dataset is acquired from [Luna Dong](http://lunadong.com/fusionDataSets.htm)'s webpage.
Data preparation is written in readFlightFiles.py
There are two of them, one is to extract two attributes, expect departure time and actual departure time.
First run the readFlightFiles_1(2)Attribute.py to generate.
There will be two generated pickle files which store the data item, value pairs for 38 sources.

An example to run this instance is
```
 argTruth.load_pickle("flight_data.p")
    argTruth.pre_processing()
    argTruth.calculate_value_confidence(num_iterations=2, method="2_estimates")
    # the methods can be : "sum", "average_log", "investment", "pooled_investment",
    # "truth_finder", "cosine", "2_estimates", "3_estimates"
    found_truth = argTruth.get_truth()
    gold_std = pickle.load(open("flight_truth.p", "rb"))
    _accuracy = argTruth.compare_truth(found_truth, gold_std)
    print(_accuracy) # Get the accuracy
```
A comparison of different methods and number of iterations.
![IMAGE](Images/CompareMethods.jpg)
