from difflib import SequenceMatcher
from functools import reduce
import json
import math
from scipy.linalg import norm
import pickle
import numpy as np


class TruthFinder:
    def __init__(self):
        self.data = []
        self.all_date = []
        self.S = set()  # set of all source
        self.V = set()  # set of all values
        self.D = set()  # set of all data items
        self.Sv = {}  # set of sources providing value v
        self.Sd = {}  # set of sources providing a value for data item d
        self.Ds = {}  # set of data items covered by source s
        self.Dv = {}  # set of data items corresponding to value v
        self.Vs = {}  # set of values provided by source s
        self.Vd = {}  # set of values provided for data item d
        self.VDs = {}  # set of values for the data items provided by sources
        self.Sv_bar = {}  # set of sources providing a distinct value from v
        self.Ts = {}
        self.Cv = {}
        self.convergence_stack = []

    @staticmethod
    def similar(a, b):  # similarity of strings
        return SequenceMatcher(None, a, b).ratio()

    @staticmethod
    def cosine_distance(a, b):
        if len(b) < len(a):
            a, b = b, a
        up = 0
        for key in a:
            a_value = a[key]
            b_value = b[key]
            up += a_value * b_value
        if up == 0:
            return 0
        return up / norm([a[key] for key in a]) / norm(([b[key] for key in b]))

    @staticmethod
    def compare_truth(__found_truth, __gold_std):
        __total = 0
        __correct = 0
        for t in found_truth:
            if t in __gold_std:
                __found = __found_truth[t]
                __gold = __gold_std[t]
                if __gold.find("empty") != -1:
                    continue
                __total += 1
                if __found == __gold:
                    __correct += 1
                # else:
                   # print(t, "gold:", __gold_std[t], "found:", __found_truth[t])
        return 1.0 * __correct / __total

    def reset(self):
        self.Ts = {}
        self.Cv = {}
        self.convergence_stack = []

    def read_json(self, file_name):
        # Read data
        with open(file_name) as data_file:
            self.data = json.load(data_file)

    def convergence(self, new_val, theta):  # cosine similarity for two arrays
        if len(self.convergence_stack) == 2:
            self.convergence_stack.pop(0)
        self.convergence_stack.append(new_val)

        if len(self.convergence_stack) == 2:
            ratio = 1 - self.cosine_distance(self.convergence_stack[0], self.convergence_stack[1])
            if ratio < theta:
                return True
            else:
                return False
        else:
            return False

    def pre_processing_by_item(self):
        for d in self.data:
            # D
            self.D.add(d)
            for s in self.data[d]:
                # S
                self.S.add(s)

                # Sd
                if d not in self.Sd:
                    self.Sd[d] = set()
                    self.Sd[d].add(s)
                else:
                    self.Sd[d].add(s)

                # Ds
                if s not in self.Ds:
                    self.Ds[s] = set()
                    self.Ds[s].add(d)
                else:
                    self.Ds[s].add(d)

                v = self.data[d][s]

                if v.find('empty') != -1:  # Ignore empty values
                    continue

                # V
                self.V.add(v)

                # Sv
                if v not in self.Sv:
                    self.Sv[v] = set()
                    self.Sv[v].add(s)
                else:
                    self.Sv[v].add(s)

                # Dv
                if v not in self.Dv:
                    self.Dv[v] = set()
                    self.Dv[v].add(d)
                else:
                    self.Dv[v].add(d)

                # Vd
                if d not in self.Vd:
                    self.Vd[d] = set()
                    self.Vd[d].add(v)
                else:
                    self.Vd[d].add(v)

                # Vs
                if s not in self.Vs:
                    self.Vs[s] = set()
                    self.Vs[s].add(v)
                else:
                    self.Vs[s].add(v)

        # fix Vd
        for d in self.D:
            if d not in self.Vd:
                self.Vd[d] = set()

        # VDs
        for s in self.S:
            for d in self.Ds[s]:
                if s not in self.VDs:
                    self.VDs[s] = set()
                    self.VDs[s].update(self.Vd[d])
                else:
                    self.VDs[s].update(self.Vd[d])

        # Vs_bar
        for v1 in self.V:
            self.Sv_bar[v1] = set()
            for v2 in self.V:
                if v1 != v2:
                    self.Sv_bar[v1].update(self.Sv[v2])

    def get_truth(self, attribute=None):
        tmp_truth = {}
        # print(self.Cv)
        for d in self.D:
            if d not in self.Vd:
                # print(d, "is not provided by any source")
                continue
            if attribute is not None:
                if d.find(attribute) == -1:
                    continue
            tmp_max = 0
            tmp_value = ""
            for v in self.Vd[d]:
                if self.Cv[v] > tmp_max:
                    tmp_max = self.Cv[v]
                    tmp_value = v
            # print("Max value for", d, "is", tmp_value)
            tmp_truth[d] = tmp_value
        return tmp_truth

    def calculate_value_confidence(self, method=None):
        # the methods can be : "sum", "average_log", "investment", "pooled_investment",
        # "truth_finder", "cosine", "2_estimates", "3_estimates"
        method_name = 'method_' + method
        func = getattr(self, method_name, lambda: "nothing")
        return func()

    def method_truth_finder(self, rho=0.6, gamma=0.1, theta=0.0000001):
        self.reset()
        for s in self.S:
            self.Ts[s] = 0.8

        theta_v = {}
        theta_star_v = {}
        while True:
            for d in self.D:
                for v in self.Vd[d]:
                    theta_v[v] = - reduce(lambda x, y: x + y,
                                          map(lambda s1: math.log(1 - self.Ts[s1]), self.Sv[v]))

                    theta_star_v[v] = theta_v[v] + rho * reduce(lambda x, y: x + y,
                                                                map(lambda v1: self.similar(v, v1), self.Vd[d]))

                    self.Cv[v] = 1.0 / (1 + math.exp(- gamma * theta_star_v[v]))

            for s in self.S:
                self.Ts[s] = reduce(lambda x, y: x + y,
                                    map(lambda v1: 1.0 * self.Cv[v1] / len(self.Vs[s]), self.Vs[s]))
            this_Ts = self.Ts.copy()
            print(this_Ts)
            if self.convergence(this_Ts, theta):
                break

    def get_w(self, s, d):
        if s in self.Sd[d]:
            return 1
        else:
            return 0

    def method_simple_lca(self, beta1=0.4, theta=0.000001):
        self.reset()
        for s in self.S:
            self.Ts[s] = 0.8

        while True:
            for d in self.D:
                C_d_sum = 0
                for v in self.Vd[d]:
                    # my modification |Vd| > 1 otherwise the second part of inference doesn't make sense
                    if len(self.Vd[d]) > 1:
                        self.Cv[v] = beta1 * reduce(lambda x, y: x * y,
                                                    map(lambda s1: self.Ts[s1] ** self.get_w(s1, d), self.Sv[v])) *\
                                 reduce(lambda x, y: x * y,
                                        map(lambda s2: ((1-self.Ts[s2]) / (len(self.Vd[d])-1)) ** self.get_w(s2, d),
                                            self.Sv_bar[v]))
                    else:
                        self.Cv[v] = beta1 * reduce(lambda x, y: x * y,
                                                    map(lambda s1: self.Ts[s1] ** self.get_w(s1, d), self.Sv[v]))

                    C_d_sum += self.Cv[v]

                for v in self.Vd[d]:  # normalization
                    self.Cv[v] = self.Cv[v] / C_d_sum

            for s in self.S:
                self.Ts[s] = reduce(lambda x, y: x + y, map(lambda v1: (self.Cv[v1]), self.Vs[s])) /\
                             reduce(lambda x, y: x + y, map(lambda d1: self.get_w(s, d1), self.D))  # *self.get_w(s, d)

            print(self.Ts)
            if self.convergence(self.Ts, theta):
                break

    def method_ltm(self, beta1, r, delta):
        pass

    def data_set_info(self):
        print("Number of source:", len(self.S))
        print("Number of data items:", len(self.D))
        print("Number of claims", reduce(lambda x, y: x+y,
                                         map(lambda d: len(self.data[d]), self.data)))
        print("Number of one claim data items:", len(list(filter(lambda x: len(self.Vd[x]) == 1, self.D))))


if __name__ == "__main__":
    gold_std = pickle.load(open("data/gold_by_item.p", "rb"))

    argTruth = TruthFinder()
    argTruth.read_json("data/test_data.json")
    argTruth.pre_processing_by_item()
    argTruth.data_set_info()
    argTruth.calculate_value_confidence(method="truth_finder")
    found_truth = argTruth.get_truth()
    _accuracy = argTruth.compare_truth(found_truth, gold_std)

    print(_accuracy)
